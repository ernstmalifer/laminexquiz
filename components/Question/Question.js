import React, { PropTypes } from 'react';
import cx from 'classnames';
import Answer from '../Answer';
import s from './Question.css';

import history from '../../core/history';
import _ from 'lodash';

class Question extends React.Component {

  static propTypes = {
  };

  constructor(props) {
    super(props);
    if(window.answers[0] !== ""){
      this.state = {selected: window.answers[0]};
    } else {
      this.state = {selected: ""};
    }
  }

  componentWillReceiveProps(nextProps) {
    
    if(window.answers[nextProps.question.id - 1] !== ""){
      this.setState({selected: window.answers[nextProps.question.id - 1]})
    } else {
      this.setState({selected: ""});
    }
  }

  updateSelected = (e) => {

    var value = e.target.getAttribute("value");

    this.setState({selected: value});
    this.props.renderParent();

    this.props.next();

    this.render();
  }


  render() {
    const { question, renderParent, length } = this.props;
    const updateSelected = this.updateSelected;
    const selected = this.state.selected;

    var answers = question.answers;

    // Shuffle fn
    // Browsersync Problems
    // var shuffledanswers = _.shuffle(answers);

    var sortedanswers = _.sortBy(answers, [function(answer) { return answer.position; }]);

    window.answers[this.props.question.id - 1] = selected;

    return (
      <article className="question">
        <h4>{question.id}. {question.question}</h4>
        <h6>{question.id}/{length}</h6>
        <hr />
        <div className={s.answers} >
        {sortedanswers.map(function(answer, i) {
          return (
            <Answer key={i} answer={answer} selected={selected} updateSelected={updateSelected}></Answer>
          );
        })}
        </div>
      </article>
    )
  }
}

export default Question;

import React, { PropTypes } from 'react';
// import cx from 'classnames';
import classNames from 'classnames/bind';
import s from './styles.css';

let cx = classNames.bind(s);

class Answer extends React.Component {

  static propTypes = {
  };

  // constructor(props) {
    // super(props);
  // }

  hey(){
    console.log('fun');
    this.props.updateSelected;
  }

  render() {
    const { answer, selected } = this.props;

    let className = cx({
      answer: true,
      active: selected == answer.answer      
    });

    var divStyle = {
      backgroundImage: 'url(' + answer.image + ')'
    };

    return (
      <label className={className} value={answer.answer} onClick={this.props.updateSelected}>
        <image style={divStyle} value={answer.answer}/><br />
        <div value={answer.answer}>
        </div>
      </label>
    )
  }
}

export default Answer;

# Laminex Quiz from [React Static Boilerplate](https://github.com/kriasoft/react-static-boilerplate/)


### Directory Layout

```shell
.
├── /components/                # Shared or generic UI components
│   ├── /Answer/                # Answer component
│   ├── /OButton/               # OButton component
│   ├── /Layout/                # Website layout component
│   ├── /Link/                  # Link component to be used insted of <a>
│   ├── /Question/              # Question component
│   └── /...                    # etc.
├── /core/                      # Core framework
│   ├── /history.js             # Handles client-side navigation
│   ├── /router.js              # Handles routing and data fetching
│   └── /store.js               # Application state manager (Redux)
├── /node_modules/              # 3rd-party libraries and utilities
├── /pages/                     # React components for web pages
│   ├── /error/                 # Error page
│   ├── /questions/             # Questions page
│   ├── /result/                # Result page
│   ├── /start/                 # Start page
│   └── /...                    # etc.
├── /public/                    # Static files such as favicon.ico etc.
│   ├── /dist/                  # The folder for compiled output
│   ├── favicon.ico             # Application icon to be displayed in bookmarks
│   ├── robots.txt              # Instructions for search engine crawlers
│   └── /...                    # etc.
├── /test/                      # Unit and integration tests
├── /utils/                     # Utility and helper classes
│── main.js                     # React application entry point
│── package.json                # The list of project dependencies and NPM scripts
│── routes.json                 # This list of application routes
│── run.js                      # Build automation script, e.g. `node run build`
└── webpack.config.js           # Bundling and optimization settings for Webpack
```

### Getting Started

**Step 1**. Make sure that you have [Node.js](https://nodejs.org/) v6 or newer installed on your
machine.

**Step 2**. Clone this repository:

```shell
$ git clone -o https://bitbucket.org/ernstmalifer/laminexquiz -b master
$ cd laminexquiz
$ npm install                   # Install project dependencies listed in package.json
```

```shell
$ npm install -g yo
$ npm install -g generator-react-static
$ mkdir MyApp
$ cd MyApp
$ yo react-static
```

**Step 3**. Compile and launch your app by running:

```shell
$ node run                      # Same as `npm start` or `node run start`
```

You can also test your app in release (production) mode by running `node run start --release` or
with HMR and React Hot Loader disabled by running `node run start --no-hmr`. The app should become
available at [http://localhost:3000/](http://localhost:3000/).


### License

Copyright © 2015-present Kriasoft, LLC. This source code is licensed under the MIT license found in
the [LICENSE.txt](https://github.com/kriasoft/react-static-boilerplate/blob/master/LICENSE.txt) file.

---
Made with ♥ by Konstantin Tarkus ([@koistya](https://twitter.com/koistya)) and [contributors](https://github.com/kriasoft/react-static-boilerplate/graphs/contributors)
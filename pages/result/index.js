/**
 * React Static Boilerplate
 * https://github.com/kriasoft/react-static-boilerplate
 *
 * Copyright © 2015-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { PropTypes } from 'react';
import Layout from '../../components/Layout';
import s from './styles.css';
import { title, html } from './index.md';

import Question from '../../components/Question';
import history from '../../core/history';
import Link from '../../components/Link';

import OButton from '../../components/OButton';

import _ from 'lodash';

class ResultPage extends React.Component {

  transition = event => {
    // console.log(history.getCurrentLocation().path);
  }

  render() {
    
    const styles = this.props.styles;
    const answers = window.answers;
    const values = _.values(answers);
    const o = _(values).reduce(function(o, s) {
        o.freq[s] = (o.freq[s] || 0) + 1;
        if(!o.freq[o.most] || o.freq[s] > o.freq[o.most])
            o.most = s;
        return o;
    }, { freq: { }, most: '' });

    console.log(o);

    console.log(window.answers);

    var style = _.find(styles, function(style){
        return style.style == o.most;
    });

    console.log(style);

    var divStyle = {
      backgroundImage: 'url(' + style.image + ')'
    };

    document.title = "Your style is " + style.style;

    return (
      <Layout className={s.content}>

        <div>
          <image style={divStyle} /><br />
        </div>
        
        <div>
        <h3>Your style is<br />{style.style}</h3>

        <div className={s.description} dangerouslySetInnerHTML={{__html:style.description}}></div>

        <OButton className={s.obutton} to={style.inspirationalgallerylink} onClick={this.transition}>Browse Inspiration Galleries</OButton>
        <OButton className={s.obutton} to={style.findoutmoreaboutyourstylelink} onClick={this.transition}>Find out more about your style</OButton>
        </div>

        
      </Layout>
    );
  }

}

export default ResultPage;
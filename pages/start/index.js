/**
 * React Static Boilerplate
 * https://github.com/kriasoft/react-static-boilerplate
 *
 * Copyright © 2015-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */


import React from 'react';
import Layout from '../../components/Layout';
import s from './styles.css';
import OButton from '../../components/OButton';
// import content from './index.html';

class StartPage extends React.Component {

  componentDidMount() {
    document.title = "Find your Style";

    window.answers = {};
    for (var i = 0; i < this.props.questions.length; ++i) {
      window.answers[i] = "";
    }
    
  }

  render() {
    return (
      <Layout className={s.content}>
        <div>
        <h2>Find your Style</h2>

        <p className={s.first}>
        Design comes in so many forms and what draws us to certain colours, textures and shapes is purely personal. Whether you like classic tones, the cool inner city urban feel or something more vintage, weve identified 12 styles to help demystify the design process. 
        </p>

        <p>
        These 12 styles have been inspired by the latest design looks throughout the world and crafted to reflect the Australian lifestyle.  Take our style quiz to find out which of our styles best matches you, your design preferences and your lifestyle. All you need to do is answer a few quick questions and well do the rest. Youll uncover our range and access interior design ideas to bring the style to life within your space. </p>

        <OButton className={s.obutton} to="/questions/1" onClick={this.transition}>Take the Quiz</OButton>
        </div>

        <div>
          <img src="images/start.png" />
        </div>


      </Layout>
    );
  }

}

export default StartPage;

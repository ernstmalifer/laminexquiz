/**
 * React Static Boilerplate
 * https://github.com/kriasoft/react-static-boilerplate
 *
 * Copyright © 2015-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React, { PropTypes } from 'react';
import Layout from '../../components/Layout';
import s from './styles.css';
import { title, html } from './index.md';

import Question from '../../components/Question';
import history from '../../core/history';
import OButton from '../../components/OButton';

class HomePage extends React.Component {

  static propTypes = {
    questions: PropTypes.array.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {question: 0, path: "", seconds: 3};
  }

  componentDidMount() {
    document.title = title;
    this.getPath();

    this.interval = setInterval(this.updateSeconds.bind(this), 1000);
  }

  updateSeconds(){
    
    if(this.state.seconds > 1){
      this.setState({seconds: this.state.seconds - 1});
    } else {
      clearInterval(this.interval);
      this.closePrompt();
    }
  }

  componentWillReceiveProps() {
    this.getPath();
    this.render();
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  getPath() {
    let path = history.getCurrentLocation().pathname;
    let pathsplit = path.split("/");
    let question = pathsplit[pathsplit.length - 1];

    this.setState({question: parseInt(question) - 1, path: path});
  }

  transition = event => {
    if(window.answers[this.state.question] == ""){
      // alert(window.answers[this.state.question]);
      alert("Please select: " + this.props.questions[this.state.question].question);
      event.preventDefault();
    }
  }

  openPrompt() {
      document.getElementById("prompt").style.height = "100%";
  }

  closePrompt() {
      clearInterval(this.interval);
      document.getElementById("prompt").style.height = "0%";
  }

  next = (e) => {
    
    var question = this.state.question;

    document.querySelector('.mdl-layout__content').scrollTop = 0;

    if (this.props.questions.length == question + 1) {
      history.push('/result');
    } else {
      history.push('/questions/' + (question + 2));
    }

  }

  backView(){
    document.querySelector('.mdl-layout__content').scrollTop = 0;
  }

  renderFromChild = (e) => {

    this.render();
  }

  render() {

    const question = this.state.question;
    const length = this.props.questions.length;

    document.title = (question + 1) + ". " + this.props.questions[question].question;

    var nextButton;

    if (this.props.questions.length == question + 1) {
      nextButton = <OButton className={s.obutton} to={"/result" }>Result</OButton>;
    } else {
      nextButton = <OButton className={s.obutton} to={"/questions/" + (question + 2) } onClick={this.transition}>Next</OButton>;
    }

    var backButton;

    if (question == 0) {
      backButton = <OButton className={s.obutton} to={"/"}  onClick={this.backView()}>Back</OButton>;
    } else {
      backButton = <OButton className={s.obutton} to={"/questions/" + (question) }  onClick={this.backView()}>Back</OButton>;
    }  

    return (
      <Layout className={s.content}>

        <span id="prompt" className={s.overlay} onClick={this.closePrompt.bind(this)}>
          <a onClick={this.closePrompt.bind(this)}>&times;</a>
          <div className={s.ocontent}>
          <h4>Please select one image<br />for each question.<br /><span>This closes in {this.state.seconds}s<br />Or Tap Anywhere to Close</span></h4>
          </div>

        </span>

        <Question question={this.props.questions[question]} renderParent={this.renderFromChild} length={length} next={this.next}></Question>
        {backButton}
      </Layout>
    );
  }

}

export default HomePage;
